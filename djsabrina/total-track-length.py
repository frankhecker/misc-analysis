import csv


def parse_duration(duration_str):
    """ Convert mm:ss to seconds """
    minutes, seconds = map(int, duration_str.split(':'))
    return minutes * 60 + seconds


def format_duration(total_seconds):
    """ Convert seconds to hh:mm:ss format """
    hours = total_seconds // 3600
    minutes = (total_seconds % 3600) // 60
    seconds = total_seconds % 60
    return f"{hours:02d}:{minutes:02d}:{seconds:02d}"


# Replace 'your_file.csv' with the path to your CSV file
file_path = 'dj-sabrina-tracks.csv'

total_duration = 0

with open(file_path, newline='') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        if row:  # checking if the row is not empty
            date, title, duration = row
            total_duration += parse_duration(duration)

formatted_total_duration = format_duration(total_duration)
print(f"Total Duration: {formatted_total_duration}")
