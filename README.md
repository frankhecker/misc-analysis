# misc-analysis

This repository contains miscellaneous public data analysis projects
that don't fit into any other category. It currently contains the
following projects, each in its own subdirectory:

- `criterion-channel`: an analysis of the films by Japanese directors
  that were available on the Criterion Channel streaming service as of
  April 2019.
- `patreon`: analysis of the distribution of Patreon project earnings
   from monthly charges to patrons for the month of December 2022.
- `manga`: analysis of various statistics related to published manga.
