---
title: "Growth in the Number of Yuri Manga Titles over Time"
author: "Frank Hecker"
date: "`r format(Sys.time(), '%Y-%m-%d')`"
output:
  html_document:
    fig_width: 6
    fig_height: 4
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

My goal in this analysis is to explore the growth in the number of
yuri manga titles (series or one-shots) over time.

For those readers not familiar with the [R statistical software][r]
and the additional [Tidyverse][tv] software I use to manipulate and
plot data, check out the various [ways to learn more about the
Tidyverse][learn].

[r]: https://www.r-project.org/
[tv]: https://www.tidyverse.org/
[learn]: https://www.tidyverse.org/learn/

## Setup

I load the following R libraries, for the purposes listed:

- tidyverse. Do general data manipulation and plotting.
- tools. Compute MD5 checksums.

```{r libraries, warning=FALSE, message=FALSE}
library("tidyverse")
library("tools")
```

## Preparing the data

### Obtaining the yuri manga data

I use a local copy of a data file containing the number of yuri manga
titles released in each year through 2024. See the “References”
section below for more information.

I check the MD5 hash values for the file, and stop if the contents are
not what are expected.

```{r hash_check}
stopifnot(md5sum("yuri-manga-per-year.csv") == "74109a3d2c1c42f9fe7d9dde472392c0")
```

### Loading the data

I load the data from the CSV file (filtering out any rows with invalid
data) and sort the date in ascending order by year.

```{r read_csv}
mpy_tb <- read_csv("yuri-manga-per-year.csv", col_types = "ii") %>%
  filter(!is.na(Year) & !is.na(Num_Manga)) %>%
  arrange(Year)
```

## Analysis

### Preliminary analysis

I do some basic exploratory data analysis, starting the total number
of yuri manga in the dataset, the earliest year covered, and the
maximum number of manga released in any year.

```{r total_manga}
total_manga <- sum(mpy_tb$Num_Manga)

earliest_year <- min(mpy_tb$Year)

max_in_year <- max(mpy_tb$Num_Manga)

year_of_max <- mpy_tb$Year[mpy_tb$Num_Manga == max_in_year]
```

There are a total of
`r format(total_manga, big.mark = ",")`
yuri manga in the dataset. The earliest year for which there is data
is
`r earliest_year`.
The most yuri manga released in any year was
`r format(max_in_year, big.mark = ",")`,
in
`r year_of_max`.

### Number of manga released per year

Now that I have my dataset of interest, I can continue my analysis,
this time by plotting the number of yuri manga released in each year.

```{r mpy_plot}
mpy_tb %>%
  ggplot(mapping=aes(x = Year, y = Num_Manga)) +
  geom_point() +
  geom_line() +
  scale_x_continuous(breaks = c(1960, 1970, 1980, 1990, 2000, 2010, 2020)) +
  scale_y_continuous(labels = scales::label_comma()) +
  xlab("Year") +
  ylab("New Yuri Manga") +
  labs(
    title = "New Yuri Manga Titles by Year (All Time)",
    subtitle = "Number of Series or One-Shots First Released in Year",
    caption = "Data source: Manga tagged GL on Anime Planet site"
  ) +
  theme_gray() +
  theme(axis.title.x = element_text(margin = margin(t = 5))) +
  theme(axis.title.y = element_text(margin = margin(r = 10))) +
  theme(plot.caption = element_text(margin = margin(t = 15), hjust = 0))
```

Since very few yuri manga were published until the 1990s, I
re-do the plot to focus on the time period from 1990 on.

```{r mpy_plot_1990}
mpy_tb %>%
  filter(Year >= 1990) %>%
  ggplot(mapping=aes(x = Year, y = Num_Manga)) +
  geom_point() +
  geom_line() +
  scale_x_continuous(breaks = c(1990, 1995, 2000, 2005, 2010, 2015, 2020)) +
  scale_y_continuous(labels = scales::label_comma()) +
  xlab("Year") +
  ylab("New Yuri Manga") +
  labs(
    title = "New Yuri Manga Titles by Year (Since 1990)",
    subtitle = "Number of Series or One-Shots First Released in Year",
    caption = "Data source: Manga tagged GL on Anime Planet site"
  ) +
  theme_gray() +
  theme(axis.title.x = element_text(margin = margin(t = 5))) +
  theme(axis.title.y = element_text(margin = margin(r = 10))) +
  theme(plot.caption = element_text(margin = margin(t = 15), hjust = 0))
```

### Cumulative number of yuri manga released by year

Another interesting statistic to look at is the cumulative number of
yuri manga released by year.

```{r mpy_tb_cumul}
mpy_tb <- mpy_tb %>%
  mutate(Cumul_Manga = cumsum(Num_Manga))

cumul_90 <- min(mpy_tb$Year[mpy_tb$Cumul_Manga > 0.1 * total_manga]) - 1
cumul_75 <- min(mpy_tb$Year[mpy_tb$Cumul_Manga > 0.25 * total_manga]) - 1
cumul_50 <- min(mpy_tb$Year[mpy_tb$Cumul_Manga > 0.5 * total_manga]) - 1
```

I plot this statistic, again looking at the period from 1990 on.

```{r mby_cumul_plot}
mpy_tb %>%
  filter(Year >= 1990) %>%
  ggplot(mapping=aes(x = Year, y = Cumul_Manga)) +
  geom_line() +
  geom_point() +
  scale_x_continuous(breaks = c(1990, 1995, 2000, 2005, 2010, 2015, 2020)) +
  scale_y_continuous(labels = scales::label_comma()) +
  xlab("Year") +
  ylab("Cumulative Yuri Manga") +
  labs(
    title = "Cumulative Yuri Manga Titles by Year",
    subtitle = "Number of Series or One-Shots First Released in or before Year",
    caption = "Data source: Manga tagged GL on Anime Planet site"
  ) +
  theme_gray() +
  theme(axis.title.x = element_text(margin = margin(t = 5))) +
  theme(axis.title.y = element_text(margin = margin(r = 10))) +
  theme(plot.caption = element_text(margin = margin(t = 15), hjust = 0))
```

More than 90% of yuri manga titles were published after
`r cumul_90`,
more than three-quarters after
`r cumul_75`,
and more than half after
`r cumul_50`.

## Appendix

### Caveats

The accuracy of the dataset, and thus of the analysis, depends on the
following factors:

- whether the source of the data has entries for all manga published
  during the timeframe in question (post-World War 2);
- whether the search properly excluded all works that are not manga in
  the strict definition of the term (i.e., comics created by Japanese
  artists and writers and published in Japan in the traditional manga
  format), and did not exclude any actual manga by mistake; and
- whether the search captured all manga with significant yuri content,
  and excluded manga lacking yuri content or with only incidental yuri
  content.

In particular, the source of the data uses a scheme devised by Western
fans in which the tag “yuri“ is used to mark works more sexual in
nature and the tag “shoujo-ai” is used to mark works focused more on
emotional relationships. The tag “GL” should include both of these
categories, but I have not confirmed this.
  
### References

Data on the number of yuri manga titles released per year were
obtained from the [Anime Planet][ap] web site using the following
procedure:

- Go to the page https://www.anime-planet.com/manga/all
- Click on the “Tags” tab.
- Click “+” on the “GL” tag.
- Click “-” on the “Light Novels”, “Manhua”, “Manwha”, “OEL”, “Web
  Novels”, and “Webtoons” tags.
- Click on the “APPLY FILTERS” button. (At the time of writing, this
  generated 49 pages of results.)
- Click on the “YEAR“ column heading to sort the listing by year.
- Count the number of manga released in each year.

[ap]: https://www.anime-planet.com/

### Environment

I used the following R environment in doing the analysis above:

```{r environment}
sessionInfo()
```

### Source code

The source code for this analysis can be found in the public code
repository
https://gitlab.com/frankhecker/misc-analysis
in the `manga` subdirectory.

This document and its source code are available for unrestricted use,
distribution and modification under the terms of the [Creative Commons
CC0 1.0 Universal (CC0 1.0) Public Domain Dedication][cc0].  Stated
more simply, you’re free to do whatever you’d like with it.

[cc0]: https://creativecommons.org/publicdomain/zero/1.0/
